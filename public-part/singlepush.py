import datetime
import requests
import json
import sys

#token = input("token:")
#head = input("Überschrift:")
#text = input("text:")
token = str(sys.argv[1])
head = str(sys.argv[2])
text = str(sys.argv[3])

print("-----------------")
print(token)
print(head)
print(text)
#store expire date in a format the server understands
expire = datetime.date.today() + datetime.timedelta(days=14)
#expire = int(pubdate + 86400)
#expire 86400 seconds (= 1 day) after message sent out
print(datetime.datetime.strftime(expire, '%Y-%m-%dT%H:%M:%S.%fZ'))
#make dictionary to be sent
plaindat={
"appid" : "s60w79.warnapp-deutschland_s60w79.warnapp-deutschland",
"expire_on": datetime.datetime.strftime(expire, '%Y-%m-%dT%H:%M:%S.%fZ'),
"token": token,
"data": {
"notification": {

"card": {

"icon": "notification",
"summary": head,
#"actions":["appid://s60w79.warnapp-deutschland"],
"body": text,
#"actions": act,
"popup": True,
"persist": True,
},
"vibrate": True,
"sound": True
}
}
}
print(plaindat)
#send notification message to the ubports push Server.
r = requests.post('https://push.ubports.com/notify', data=json.dumps(plaindat), headers={"Content-type": "application/json"})
print(r.text)

print("succeeded")

  
 
