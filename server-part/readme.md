# Server Part

## Function:

The server checks the lines of rss list and looks if a rss message was sent since the last check (with the help of the last column, see readme file in directory above). 
If there is a new feed message in one of the rss feeds, all users, subscribed the feed (via rss-token) will get a push notification over the UT Push SRV.

## Content Generation

The Properties of vibrate, etc are static.
The Push Message's Text is Gnerated from the Title of the RSS entry.
The Heading of the Message is adjusted in the following priority

1. a Heading is given from the RSSlist registration, use them
2. If there isn't (DB Field is empty or Aterisk) , use the Title of RSS Feed
3. If a title is not given, use the Fallback: *New RSS Message*

The Icon of the message uses this priority:

1. If an Icon is given from the RSSlist registration, use them
2. If the RSS Feed has an Image, use it's href
3. If the RSS Feed has an Icon, use them
4. If non of the cases is matching, use Fallback: *rssreader-app-symbolic*

**Actions** are to be continued... There is also a DB Field, where AppIDs can be registered, it just has to be implemented in the server Part.

## Usage

This python script runs only ONCE it's started. That means man has to change the script, or (and I prefer this solution, because the debug settings are better throw this): set a crontab entry or Sytemd Timer which executes the script each three (or even each - ) minutes.

### e.g. crontab

`crontab -e`

Add the following line at the And of the File:

` 3 * * * * python3 /home/NAME/WORKINGDIR/srv.py`

Then save them. This will lookup the RSS feeds every 3 Minutes.
Bew aware that one run takes less then the update interval.

