import time
import feedparser

import mysql.connector
import datetime
import requests
import json
print("starting")
#Database connnection section
mydb = mysql.connector.connect(
host="127.0.0.1",
user="CENSORED",
password="CENSORED",
database="devel"
)

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM rsslist ")
failed = ""
table = mycursor.fetchall()
print("start scan")
#go throw each line in the feed list table | the feed list table contains all feeds, ever been subscribed by a user.
for line in table:
    #
    #line[0] is the adress of the rss feed which is handled now. after parsing the xml turns into an array with the masages as elements of the array
    rss = feedparser.parse(line[0])
    #greps the first entry, which is also the latest entry.z
    if len(rss.entries) > 0:
        warn = rss.entries[0]
        ##stores the published as a string
        string = warn.published
        #checks in which format the string is and transforms it into a unix-timestamp
        if string[4] == '-':
            pubdate = time.mktime(datetime.datetime.strptime(string,"%Y-%m-%dT%H:%M:%S.%f%z").timetuple())
        else:
            pubdate = time.mktime(datetime.datetime.strptime(string,"%a, %d %b %Y %H:%M:%S %z").timetuple())
        #line [1] (from the table rsslist) also a timestamp, which stands for the last time, this feed caused a push notification. so if the last sent out push notification was BEFORE the latest message published, there was a new message not handled yet, and a push notification is to send out.
        if pubdate > float(line[1]):
            #updates line[1] to the published date so in the next check there wont be a push message
            operator = mydb.cursor()
            squp = "UPDATE `rsslist` SET last = "+str(pubdate)+" WHERE last="+str(line[1])+" AND rss='"+line[0]+"';"
            operator.execute(squp)
            mydb.commit()
            #find token
            subcursor = mydb.cursor()
            subcursor.execute("SELECT * FROM `token-rss` WHERE rss='"+line[0]+"'")
            subtable = subcursor.fetchall()
            
            for token in subtable:
                #actually nonsense to use for but yey
                
                #check if the feed is a warn message for the app 'warnapp' deutschland' to adjust the push heading, because if not it's just a rss message
                heading = line[3]
                if heading == "" or heading == "*":
                    #if the heading hasn't been pre-defined by the App -> Read out RSS's title and use it as heading.
                    if "title" in rss.feet:
                        #if no heading given by the app and rss feed has a title -> use title as hadding
                        heading = rss.feed.title
                    else:
                        # If not, use Fallback
                        heading = "New RSS Message"
                icon = line[2]
                if icon == "" or icon == "*":
                    if "image" in rss.feed:
                        if "href" in rss.feed.image:
                            # if there is an feed image, use it as icon.
                            icon = rss.feed.image.href
                    else:
                        if "icon" in rss.feed:
                            # if there isn't a feed image, but an feed icon, use them as push icon.
                            icon = rss.feed.icon
                        else:
                            # Fallback if no Icon has been found.
                            icon = "rssreader-app-symbolic"
                #store expire date in a format the server understands
                expire = datetime.date.today() + datetime.timedelta(days=2)
                #expire = int(pubdate + 86400)
                #expire 86400 seconds (= 1 day) after message sent out
                #make dictionary to be sent
                plaindat={
                "appid" : "s60w79.warnapp-deutschland_s60w79.warnapp-deutschland",
                "expire_on": datetime.datetime.strftime(expire, '%Y-%m-%dT%H:%M:%S.%fZ'),
                "token": token[0],
                "data": {
                "notification": {
                
                "card": {
                
                "icon": icon,
                "summary": heading,
                #"actions":["appid://s60w79.warnapp-deutschland"],
                "body": warn.title,
                #"actions": act,
                "popup": True,
                "persist": True,
                "timestamp": int(pubdate)
                },
                "vibrate": True,
                "sound": True
                }
                }
                }
                #send notification message to the ubports push Server.
                r = requests.post('https://push.ubports.com/notify', data=json.dumps(plaindat), headers={"Content-type": "application/json"})
                if r.status_code == 400:
                    #invalid token
                    failed = failed+"\n"+token[0]+"\n"+r.responseText
                    print(token[0])
print("safing failed requests")
log = open("PATH", "w")
log.write(failed)
print("succeeded")

    
