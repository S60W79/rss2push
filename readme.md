# RSS2Token (UT)

Thoose programs are to establish servers for apps which would like to subscribe push notifications for rss feeds.
one part of the project is available on a http/php server, the other part runs as a background python script on a server but should be separated from teh public http part. You'll see furher descriptions concerning the parts in readmes in each directory.

Here some information, importend for booth parts:

## what is a Push token?

A push token is a special ID. the Identifier is unique, it exists only one time and marks ONE app on ONE device.
An App, which allowes Push notifications can demand a token throw an UT Library.
For more Information, see: https://docs.ubports.com/en/latest/appdev/guides/pushnotifications.html

## Database Structure and meaning

The Database has two tables: rsslist and token-rss.

### token-rss

The table consists of three columns: token, rss and datum (please excuse the last name, it just means 'date' in english)

#### rss...
string,
stands for the feed adress somebody has subscribed of course
#### token
string,
stands for the identity who or which app has subscribed the feed. to gether thoose two parameters say WHO has subscribed WHAT
#### datum
unix time stamp/int
(eng. date), says when the user did use the feed for the last time. That allows the server hoster to just delete entries which are older than 3 years for example. I would recommend to send the user a push message manually before doing so, seriously I don't know if such steps are neccesarry at all, but it's better to prepare for a possibly data overload (;
#### appID
AppId of the registering app.
#### Icon
Icon to send when a new RSS message has been detected
#### Heading
Heading to be used when a new RSS message has been detected.

### rss list

this list says which rss feeds(string) are used by the ENTIRE systems (means if ANY user has subscribed a feed, it could be found here) and when the latest feed was published (-> last)
last ist a unix time stamp/int


### Censored Data 

Of course all the login data has been censored before uploading it in github.
Here is what has to be entried to establish a connection to the Database Server:
NAME ... username of the database
PWD ...  Password of the database
DBNAME   Name of the database
